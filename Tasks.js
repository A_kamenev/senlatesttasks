/**
*	Task1
*/
function task1 () {
	//Get N by user
	var N = parseInt(prompt('Задание 1. Укажите число N - верхнюю границу интервала', 100));
	//Minimal validation
	if (!N || N < 2) {
		alert('Вы ввели некорректные значения. Простое число — натуральное (целое положительное) число, имеющее ровно два РАЗЛИЧНЫХ НАТУРАЛЬНЫХ делителя — единицу и самого себя');
		return;
	}

	var result = new Array();
	for (var i = 2; i <= N; i++) {
		//Is i simple?
		var isSimple = true;
		for (var j = 2; j < i; j++) {
			if (i % j === 0) {
				isSimple = false;
				break;
			}
		}
		isSimple? result.push(i) : '';
	}

	alert('Результат:\n' + result.join('\n'));
}

/**
*	Task2
*/
function task2 () {
	//Get N by user
	var N = parseInt(prompt('Задание 2. Укажите число N - верхнюю границу интервала', 100));
	//Minimal validation
	if (!N || N < 1) {
		alert('Вы ввели некорректные значения. Числа Фибоначчи - элементы числовой последовательности, в которой первые два числа равны либо 1 и 1, либо 0 и 1, а каждое последующее число равно сумме двух предыдущих чисел');
		return;
	}

	var result = new Array();
	result[0] = 1;
	result[1] = 1;
	result = task2recur(N, result);

	alert('Результат:\n' + result.join('\n'));
	
	//Recursion
	function task2recur (n, res) {
		var fibSum = res[res.length-1]+res[res.length-2];
		if (fibSum <= n) {
			res.push(fibSum);
			task2recur(n, res);
		}
		
		return res;
	}
}

/**
*	Task3
*/
function task3() {
	//Get coords by user
	var coords1Str = prompt('Задание 3. Укажите начальные и конечные координаты ПЕРВОГО отрезка в формате (x1;y1),(x2;y2)', '(0;0),(100;100)'),
		coords2Str = prompt('Укажите начальные и конечные координаты ВТОРОГО отрезка в формате (x1;y1),(x2;y2)', '(0;100),(100;0)');
	
	//RegExp validation
	var reg = /\(-?[0-9]*;-?[0-9]*\),\(-?[0-9]*;-?[0-9]*\)/;
	if (!reg.test(coords1Str) || !reg.test(coords2Str)) {
		alert('Неверный формат. Попробуйте еще раз');
		return;
	}

	//Change user-friendly format to comfort format
	coords1Str = coords1Str.replace(/\(|\)/g, "");
	coords2Str = coords2Str.replace(/\(|\)/g, "");

	var dots1Str = coords1Str.split(","),
		dots2Str = coords2Str.split(",");
	
	var line1Start = dots1Str[0].split(";"),
		line1End = dots1Str[1].split(";"),
		line2Start = dots2Str[0].split(";"),
		line2End = dots2Str[1].split(";");
	
	var line1 = {
		start: {
			x: line1Start[0] <= line1End[0] ? line1Start[0] : line1End[0],
			y: line1Start[0] <= line1End[0] ? line1Start[1] : line1End[1]
		},
		end: {
			x: line1Start[0] <= line1End[0] ? line1End[0] : line1Start[0],
			y: line1Start[0] <= line1End[0] ? line1End[1] : line1Start[1]
		}
	}
	var line2 = {
		start: {
			x: line2Start[0] <= line2End[0] ? line2Start[0] : line2End[0],
			y: line2Start[0] <= line2End[0] ? line2Start[1] : line2End[1]
		},
		end: {
			x: line2Start[0] <= line2End[0] ? line2End[0] : line2Start[0],
			y: line2Start[0] <= line2End[0] ? line2End[1] : line2Start[1]
		}
	};

	var isIntersect = true;
	//y = kx + b
	
	//Coefficients k
	var k1 = (line1.end.x-line1.start.x)/(line1.end.y-line1.start.y);
	var k2 = (line2.end.x-line2.start.x)/(line2.end.y-line2.start.y);

	//is parallel?
	if (k1 === k2) {
		isIntersect = false;
		isIntersectAlert(isIntersect);
		return;
	}

	//free members
	var b1 = line1.start.y - k1 * line1.start.x;
	var b2 = line2.start.y - k2 * line2.start.x;

	//Solve the system of equations and get the intersection point
	var intersectX = (b2-b1)/(k1-k2);
	var intersectY = k1*intersectX + b1;
	
	//is intersection in line?
	if (((line1.start.x <= intersectX) && (line1.end.x >= intersectX) && (line2.start.x <= intersectX) && (line1.end.x >= intersectX))
	 || ((line1.start.y <= intersectY) && (line1.end.y >= intersectY) && (line2.start.y <= intersectY) && (line1.end.y >= intersectY))) {
		isIntersect = true;
	}
	else {
		isIntersect = false;
	}

	isIntersectAlert(isIntersect);
	
	function isIntersectAlert(isI) {
		alert ("Отрезки "+(isI? "" : "НЕ")+" пересекаются");
		return;
	}
}

/**
*	Task4
*/
function task4 () {
	//Get numbers by user
	var numsStr = prompt('Задание 4. Введите два числа через пробел', '99 63');

	//RegExp validation
	var reg = /[0-9]*\s[0-9]*/;
	if (!reg.test(numsStr)) {
		alert('Неверный формат. Попробуйте еще раз');
		return;
	}

	var numbers = numsStr.split(" ");

	//User numbers
	var numMax = Math.max(numbers[0],numbers[1]),
		numMin = Math.min(numbers[0],numbers[1]);

	//NOD
	var NOD = getNOD(numMax, numMin);
	
	//NOK
	var NOK = (numMax*numMin)/NOD;

	alert("Результат:\nНОД: "+NOD+"\nНОК: "+NOK);

	//Recursion NOD
	function getNOD (dividend, divider) {
		var remainder = dividend % divider;
		return remainder > 0 ? getNOD(divider, remainder) : divider;
	}
}

/**
*	Task5
*/
function task5() {
	//Get text by user
	var userText = prompt('Задание 5. Введите слово или фразу', 'наворован');
	
	//Filtration
	userText = userText.replace(/\s*/g, "").toLowerCase();

	if (!userText) {
		alert('Вы ввели некорректные значения. Ничего не выйдет');
		return;
	}

	var isPalindrome = true;
	var symbols = userText.split("");
	for (var i = 0; i < symbols.length/2; i++) {
		if (symbols[i] !== symbols[symbols.length-i-1]) {
			isPalindrome = false;
			break;
		}
	}

	alert('Слово (фраза) '+(!isPalindrome ? 'НЕ' : '')+' ЯВЛЯЕТСЯ палиндромом');
}

/**
*	Task6
*/
function task6 () {
	//Get text by user
	var userText = prompt('Задание 6. Введите строку', 'q1w2e3r4t5y6');

	if (!userText) {
		alert('Вы ввели некорректные значения. Ничего не выйдет');
		return;
	}

	var result = userText.replace(/[0-9]*/g, "");
	alert('Результат: '+result);
}